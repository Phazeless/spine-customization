using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using Spine;
using Spine.Unity.AttachmentTools;

public class Color_Picker : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Color_Picker m_Instance;
    //===== STRUCT =====
    public enum e_BodyType {
        HAIR = 0,
        BODY,
        FACE,
        HAND,
        PANTS,
        LEG
    }
    //===== PUBLIC =====
    public Skeleton m_Skeleton;
    public Attachment m_Attachment;
    public SkeletonAnimation m_SkeletonAnimation;
    public Material m_SourceMaterial;


    [SpineAttachment(currentSkinOnly: true, slotField: "Body_Skin")]
    public List<string> m_BodySkin;

    public Sprite m_BodySprite;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        //f_Combine();
    }

    void Update(){
        f_Combine();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_Combine() {
        //m_Skeleton = m_SkeletonAnimation.skeleton;
        //m_SourceMaterial = m_SkeletonAnimation.skeletonDataAsset.atlasAssets[0].PrimaryMaterial;
        ////Debug.Log(m_Skeleton.FindSlotIndex("Body_Skin"));
        //Debug.Log(m_Skeleton.GetAttachment(m_Skeleton.FindSlotIndex("Body_Skin"), "mc_cowo_2nd_skin/body 2nd"));
        ////m_Attachment = m_Skeleton.GetAttachment(m_Skeleton.FindSlotIndex("Body_Skin"), "mc_cowo_2nd_skin/body 2nd");
        ////Attachment m_NewAttachment = m_Attachment.GetRemappedClone(
        ////    m_BodySprite,
        ////    m_SourceMaterial,
        ////    true,
        ////    true,
        ////    false,
        ////    true,
        ////    false);
        //m_Skeleton.FindSlot("Body_Skin").Attachment = null;
        //Skin m_Skin = new Skin("Naama");
        //m_Skin.SetAttachment(m_Skeleton.FindSlotIndex("Body_Skin"), "mc_cowo_2nd_skin/body 2nd", m_Attachment);
       
        //m_Skeleton.SetSkin(m_Skin);
        //m_Skeleton.SetSlotsToSetupPose();
        m_SkeletonAnimation.skeleton.SetAttachment("Body_Skin",m_BodySkin[0]);
        //m_SkeletonAnimation.AnimationState.Apply(m_Skeleton);
        //m_SkeletonAnimation.Update(0);
    }

    public void f_NextSkin(int p_Index) {
        if (p_Index == (int)e_BodyType.HAIR) {

        } else if (p_Index == 1) {

        }
    }
}
 