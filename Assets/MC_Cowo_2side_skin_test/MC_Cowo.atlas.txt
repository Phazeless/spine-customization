
MC_Cowo.png
size: 1611,235
format: RGBA8888
filter: Linear,Linear
repeat: none
mc_cowo_1st_skin/body
  rotate: false
  xy: 205, 52
  size: 137, 181
  orig: 137, 181
  offset: 0, 0
  index: -1
mc_cowo_1st_skin/hand l 2
  rotate: true
  xy: 205, 2
  size: 48, 129
  orig: 52, 134
  offset: 1, 3
  index: -1
mc_cowo_1st_skin/hand l1
  rotate: false
  xy: 1392, 114
  size: 61, 119
  orig: 63, 123
  offset: 1, 2
  index: -1
mc_cowo_1st_skin/hand r 1
  rotate: true
  xy: 912, 24
  size: 61, 120
  orig: 63, 124
  offset: 1, 2
  index: -1
mc_cowo_1st_skin/hand r 2
  rotate: false
  xy: 1034, 3
  size: 51, 99
  orig: 53, 102
  offset: 1, 1
  index: -1
mc_cowo_1st_skin/hipp
  rotate: true
  xy: 1113, 107
  size: 126, 104
  orig: 127, 104
  offset: 0, 0
  index: -1
mc_cowo_1st_skin/leg l 1
  rotate: true
  xy: 764, 12
  size: 66, 146
  orig: 70, 148
  offset: 1, 1
  index: -1
mc_cowo_1st_skin/leg r 1
  rotate: true
  xy: 764, 12
  size: 66, 146
  orig: 70, 148
  offset: 1, 1
  index: -1
mc_cowo_1st_skin/leg l 2
  rotate: false
  xy: 978, 104
  size: 43, 129
  orig: 45, 131
  offset: 1, 1
  index: -1
mc_cowo_1st_skin/leg l 3
  rotate: true
  xy: 1507, 64
  size: 51, 50
  orig: 55, 53
  offset: 1, 2
  index: -1
mc_cowo_1st_skin/leg r 2
  rotate: false
  xy: 1023, 104
  size: 43, 129
  orig: 45, 131
  offset: 1, 1
  index: -1
mc_cowo_1st_skin/leg r 3
  rotate: true
  xy: 1512, 11
  size: 51, 50
  orig: 55, 53
  offset: 1, 2
  index: -1
mc_cowo_2nd_skin/body 2nd
  rotate: false
  xy: 344, 52
  size: 125, 181
  orig: 137, 181
  offset: 6, 0
  index: -1
mc_cowo_2nd_skin/hand l1 2nd
  rotate: true
  xy: 1392, 3
  size: 53, 118
  orig: 57, 120
  offset: 2, 1
  index: -1
mc_cowo_2nd_skin/hand l2 2nd
  rotate: false
  xy: 1254, 6
  size: 46, 104
  orig: 48, 106
  offset: 1, 1
  index: -1
mc_cowo_2nd_skin/hand r1 2nd
  rotate: false
  xy: 1340, 113
  size: 50, 120
  orig: 52, 124
  offset: 1, 1
  index: -1
mc_cowo_2nd_skin/hand r2 2nd
  rotate: false
  xy: 1302, 9
  size: 46, 101
  orig: 50, 104
  offset: 2, 1
  index: -1
mc_cowo_2nd_skin/hipps 2nd
  rotate: false
  xy: 1127, 2
  size: 125, 103
  orig: 128, 105
  offset: 1, 1
  index: -1
mc_cowo_2nd_skin/leg l1 2nd
  rotate: false
  xy: 842, 87
  size: 66, 146
  orig: 70, 148
  offset: 1, 1
  index: -1
mc_cowo_2nd_skin/leg l2 2nd
  rotate: false
  xy: 1068, 104
  size: 43, 129
  orig: 45, 131
  offset: 1, 1
  index: -1
mc_cowo_2nd_skin/leg r2 2nd
  rotate: false
  xy: 1068, 104
  size: 43, 129
  orig: 45, 131
  offset: 1, 1
  index: -1
mc_cowo_2nd_skin/leg l3 2nd
  rotate: true
  xy: 1559, 65
  size: 51, 50
  orig: 55, 53
  offset: 1, 2
  index: -1
mc_cowo_2nd_skin/leg r3 2nd
  rotate: true
  xy: 1559, 65
  size: 51, 50
  orig: 55, 53
  offset: 1, 2
  index: -1
mc_cowo_2nd_skin/leg r1 2nd
  rotate: false
  xy: 910, 87
  size: 66, 146
  orig: 70, 148
  offset: 1, 1
  index: -1
mc_cowo_Default_skin/body def
  rotate: false
  xy: 471, 62
  size: 117, 171
  orig: 118, 172
  offset: 0, 1
  index: -1
mc_cowo_Default_skin/eyes 1 Copy
  rotate: false
  xy: 642, 9
  size: 120, 69
  orig: 129, 73
  offset: 6, 3
  index: -1
mc_cowo_Default_skin/eyes 2  Copy
  rotate: false
  xy: 1392, 58
  size: 113, 54
  orig: 117, 59
  offset: 1, 4
  index: -1
mc_cowo_Default_skin/hair
  rotate: false
  xy: 590, 80
  size: 250, 153
  orig: 255, 155
  offset: 4, 1
  index: -1
mc_cowo_Default_skin/hand  l1 def
  rotate: false
  xy: 1500, 117
  size: 50, 116
  orig: 52, 118
  offset: 1, 1
  index: -1
mc_cowo_Default_skin/hand l2 def
  rotate: false
  xy: 1350, 10
  size: 40, 101
  orig: 42, 103
  offset: 1, 1
  index: -1
mc_cowo_Default_skin/hand l3 def
  rotate: false
  xy: 599, 6
  size: 41, 72
  orig: 44, 75
  offset: 2, 2
  index: -1
mc_cowo_Default_skin/hand r1 def
  rotate: false
  xy: 1552, 118
  size: 45, 115
  orig: 48, 120
  offset: 2, 2
  index: -1
mc_cowo_Default_skin/hand r2 def
  rotate: false
  xy: 1087, 6
  size: 38, 96
  orig: 40, 100
  offset: 1, 2
  index: -1
mc_cowo_Default_skin/hand r3 def
  rotate: true
  xy: 336, 3
  size: 47, 80
  orig: 51, 83
  offset: 1, 2
  index: -1
mc_cowo_Default_skin/head
  rotate: true
  xy: 2, 27
  size: 206, 201
  orig: 208, 203
  offset: 1, 1
  index: -1
mc_cowo_Default_skin/hipp def
  rotate: false
  xy: 1219, 112
  size: 119, 121
  orig: 120, 121
  offset: 0, 0
  index: -1
mc_cowo_Default_skin/leg l1 def
  rotate: true
  xy: 471, 3
  size: 57, 126
  orig: 59, 129
  offset: 1, 2
  index: -1
mc_cowo_Default_skin/leg r1 def
  rotate: true
  xy: 471, 3
  size: 57, 126
  orig: 59, 129
  offset: 1, 2
  index: -1
mc_cowo_Default_skin/leg l2 def
  rotate: false
  xy: 1455, 115
  size: 43, 118
  orig: 45, 120
  offset: 1, 1
  index: -1
mc_cowo_Default_skin/leg r2 def
  rotate: false
  xy: 1455, 115
  size: 43, 118
  orig: 45, 120
  offset: 1, 1
  index: -1
mc_cowo_Default_skin/leg l3 def
  rotate: false
  xy: 418, 3
  size: 51, 47
  orig: 55, 49
  offset: 1, 1
  index: -1
mc_cowo_Default_skin/leg r3 def
  rotate: false
  xy: 418, 3
  size: 51, 47
  orig: 55, 49
  offset: 1, 1
  index: -1
